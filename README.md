# script for LXPLUS

This repository contains scripts for LXPLUS.

Contents:
* clean.sh
    * Execute this before commit so all "\*~" files are deleted.
* command
* cron
* include
* README.md
* src


## command

This direcoty contains shell scirpts to be used as a normal command.
Install directory is "~/bin".
Install script is "install_command.sh".

## shell_script

Shell scripts **not used as a normal command** are in this directory.

## cron

Descriptions for (a)crontab are contained in this directory.

## src

This directory contains sources of C/C++.
Shell scripts in "command" use these application.
Executable files are put in this directory.
Headers files are in "include" directory.

## include

Header files for sources of C\C++ in "src" are contained.
