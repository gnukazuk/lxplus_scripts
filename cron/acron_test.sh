#!/bin/bash

log="${HOME}/env_acron.txt"
source ~/.bash_profile > /dev/null 2>&1
source ~/.bashrc > /dev/null 2>&1

env >> ${log}
phast 2>&1 >> ${log}

