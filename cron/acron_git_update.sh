#!/bin/bash

source ~/.bash_profile > /dev/null 2>&1
source ~/.bashrc > /dev/null 2>&1

log_dir="${HOME}/etc/cron/log/`date +"%Y%m"`/"
log_file="`date +"%Y%m%d_%H%M%S"`.log"
log="${log_dir}/${log_file}"
home="${HOME}"
git_parent_dir="${home}/private"

if [ ! -e "${log_dir}" ]
then
    mkdir ${log_dir}
fi

log_prev="${log_dir}`ls -1t ${log_dir} | head -n 1 `"
ls $log_prev

touch ${log}

echo -e "========== MyLibrary  ===========" >> ${log} 2>&1
cd ${git_parent_dir}/MyLibrary
git_out=`git pull`

echo "${git_out}" >> ${log} 2>&1
if [ "${git_out}" != "Already up-to-date." ]
then
    ./all_in_one.sh >> ${log} 2>&1
    
fi


echo -e "\n========== TBrowser  ===========" >> ${log} 2>&1
cd ../TBrowser
git_out=`git pull`

echo "${git_out}" >> ${log} 2>&1
if [ "${git_out}" != "Already up-to-date." ]
then
    cd src
    make clean >> ${log} 2>&1
    make -j >> ${log} 2>&1
    make install >> ${log} 2>&1
    make clean >> ${log} 2>&1
    cd ..
fi

echo -e "\n========== RootFileViewer  ===========" >> ${log} 2>&1
cd ../RootFileViewer
git_out=`git pull`

echo "${git_out}" >> ${log} 2>&1
if [ "${git_out}" != "Already up-to-date." ]
then
    cd src
    make clean >> ${log} 2>&1
    make -j >> ${log} 2>&1
    make install >> ${log} 2>&1
    make clean >> ${log} 2>&1
    cd ..
fi

echo -e "\n========== Analysis (Phast)  ===========" >> ${log} 2>&1
#cd ${git_parent_dir}/phast/analysis
cd ${home}/public/phast/analysis
git_out=`git pull`

echo "${git_out}" >> ${log} 2>&1
if [ "${git_out}" != "Already up-to-date." ]
then
    echo "something chaned. need to sync?" >> ${log}
fi

# if a size of this log is same as the previous log, remove this log
size_prev=`cat ${log_prev} | wc -c`
size_current=`cat ${log} | wc -c`

if [ "${size_current}" -eq "${size_prev}" ]
then
    rm ${log}
fi
