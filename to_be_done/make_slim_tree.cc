#define __CINT__

#ifdef __linux__
//#include "/home/gnukazuk/local/MyLibrary/include/MyLibrary.hh"
#include "/afs/cern.ch/user/g/gnukazuk/private/MyLibrary/include/MyLibrary.hh"
#elif __APPLE__
#include "/Users/nukazuka/local/MyLibrary/include/MyLibrary.hh"
#endif

void make_slim_tree()
{

  string data_path = "RD2014_t2_UE300.root";
  TTree* tr_original = GetTree( data_path , "tree" );

  string save_path = Replace( data_path , ".root" , "_slim.root" );
  TFile* tf = new TFile( save_path.c_str() , "RECREATE" );

  TTree* tr_new = new TTree( "tree_slim", "tree" );

  double im , r_vertex
    , p_beam_abs , p_gamma_abs , pt_gamma
    , x_pi , x_p , xf
    , theta_cs , phi_cs
    , phi_cs_acos , phi_cs_asin , cos2theta , sin2theta_cosphi , sin_sq_theta_cos_2phi;

  im = r_vertex
    = p_beam_abs = p_gamma_abs = pt_gamma
    = x_pi = x_p = xf
    = theta_cs = phi_cs
    = phi_cs_acos = phi_cs_asin = cos2theta = sin2theta_cosphi = sin_sq_theta_cos_2phi
    = -1;

 //  double *p_beam, *p_gamma, *p_mupos, *p_munega;
  double pos_vertex[3], p_beam[3], p_gamma[3], p_mupos[3], p_munega[3];

  bool bl_mt_last, bl_ot_last, bl_last_last,
    bl_mt_last_valid, bl_ot_last_valid, bl_last_last_valid,
    bl_dimuontrigger,
    bl_nh3, bl_w, bl_al,
    bl_r_cut,
    bl_trigger_validation_cut, bl_image_cut;

  bl_mt_last = bl_ot_last = bl_last_last =
    bl_mt_last_valid = bl_ot_last_valid = bl_last_last_valid =
    bl_dimuontrigger =
    bl_nh3 = bl_w = bl_al =
    bl_r_cut =
    bl_trigger_validation_cut = bl_image_cut
  = false;

  tr_original->SetBranchAddress	( "IM", &im );
  tr_new->Branch		( "IM", &im , "IM/D" );

  tr_original->SetBranchAddress	( "Pos_vertex", pos_vertex );
  tr_new->Branch		( "Pos_vertex", pos_vertex , "Pos_vertex[3]/D" );

  tr_original->SetBranchAddress	( "R_vertex", &r_vertex  );
  tr_new->Branch		( "R_vertex", &r_vertex  , "R_vertex/D" );

  tr_original->SetBranchAddress	( "P_beam", p_beam );
  tr_new->Branch		( "P_beam", p_beam , "P_beam[3]/D" );

  tr_original->SetBranchAddress	( "P_beam_abs", &p_beam_abs );
  tr_new->Branch		( "P_beam_abs", &p_beam_abs , "P_beam_abs/D" );

  tr_original->SetBranchAddress	( "P_gamma", p_gamma);
  tr_new->Branch		( "P_gamma", p_gamma, "P_gamma[3]/D" );

  tr_original->SetBranchAddress	( "P_gamma_abs", &p_gamma_abs );
  tr_new->Branch		( "P_gamma_abs", &p_gamma_abs , "P_gamma_abs/D" );

  tr_original->SetBranchAddress	( "Pt_gamma", &pt_gamma  );
  tr_new->Branch		( "Pt_gamma", &pt_gamma  , "Pt_gamma/D" );

  tr_original->SetBranchAddress	( "P_mupos", p_mupos);
  tr_new->Branch		( "P_mupos", p_mupos, "P_mupos[3]/D" );
  
  tr_original->SetBranchAddress	( "P_munega", p_munega  );
  tr_new->Branch		( "P_munega", p_munega  , "P_munega[3]/D" );
  
  tr_original->SetBranchAddress	( "X_pi", &x_pi);
  tr_new->Branch		( "X_pi", &x_pi, "X_pi/D" );

  tr_original->SetBranchAddress	( "X_p", &x_p );
  tr_new->Branch		( "X_p", &x_p , "X_p/D" );

  tr_original->SetBranchAddress	( "Xf", &xf  );
  tr_new->Branch		( "Xf", &xf  , "Xf/D" );

  tr_original->SetBranchAddress	( "Theta_CS", &theta_cs  );
  tr_new->Branch		( "Theta_CS", &theta_cs  , "Theta_cs/D" );

  tr_original->SetBranchAddress	( "Phi_CS", &phi_cs );
  tr_new->Branch		( "Phi_CS", &phi_cs , "Phi_cs/D" );

  tr_original->SetBranchAddress	( "Phi_CS_acos", &phi_cs_acos );
  tr_new->Branch		( "Phi_CS_acos", &phi_cs_acos , "Phi_cs_acos/D" );

  tr_original->SetBranchAddress	( "Phi_CS_asin", &phi_cs_asin );
  tr_new->Branch		( "Phi_CS_asin", &phi_cs_asin , "Phi_cs_asin/D" );

  tr_original->SetBranchAddress	( "Cos2theta", &cos2theta );
  tr_new->Branch		( "Cos2theta", &cos2theta , "Cos2theta/D" );

  tr_original->SetBranchAddress	( "Sin2theta_Cosphi", &sin2theta_cosphi );
  tr_new->Branch		( "Sin2theta_Cosphi", &sin2theta_cosphi , "Sin2theta_Cosphi/D" );

  tr_original->SetBranchAddress	( "Sin_sq_theta_cos_2phi", &sin_sq_theta_cos_2phi );
  tr_new->Branch		( "Sin_sq_theta_cos_2phi", &sin_sq_theta_cos_2phi , "Sin_sq_theta_cos_2phi/D" );

  tr_original->SetBranchAddress	( "MT_LAST", &bl_mt_last);
  tr_new->Branch		( "MT_LAST", &bl_mt_last, "mt_last/O" );

  tr_original->SetBranchAddress	( "OT_LAST", &bl_ot_last);
  tr_new->Branch		( "OT_LAST", &bl_ot_last, "ot_last/O" );

  tr_original->SetBranchAddress	( "LAST_LAST", &bl_last_last );
  tr_new->Branch		( "lAST_LAST", &bl_last_last , "last_last/O" );

  tr_original->SetBranchAddress	( "MT_LAST_valid", &bl_mt_last_valid );
  tr_new->Branch		( "MT_LAST_valid", &bl_mt_last_valid , "mt_last_valid/O" );

  tr_original->SetBranchAddress	( "OT_LAST_valid", &bl_ot_last_valid );
  tr_new->Branch		( "OT_LAST_valid", &bl_ot_last_valid , "ot_last_valid/O" );

  tr_original->SetBranchAddress	( "LAST_LAST_valid", &bl_last_last_valid );
  tr_new->Branch		( "LAST_LAST_valid", &bl_last_last_valid , "last_last_valid/O" );

  tr_original->SetBranchAddress	( "DimuonTrigger", &bl_dimuontrigger );
  tr_new->Branch		( "DimuonTrigger", &bl_dimuontrigger , "DimuonTrigger/O" );

  tr_original->SetBranchAddress	( "NH3", &bl_nh3 );
  tr_new->Branch		( "NH3", &bl_nh3 , "NH3/O" );

  tr_original->SetBranchAddress	( "W", &bl_w);
  tr_new->Branch		( "W", &bl_w, "W/O" );

  tr_original->SetBranchAddress	( "Al", &bl_al  );
  tr_new->Branch		( "Al", &bl_al  , "Al/O" );

  tr_original->SetBranchAddress	( "R_cut", &bl_r_cut  );
  tr_new->Branch		( "R_cut", &bl_r_cut  , "R_cut/O" );

  tr_original->SetBranchAddress	( "Trigger_validation_cut", &bl_trigger_validation_cut );
  tr_new->Branch		( "Trigger_validation_cut", &bl_trigger_validation_cut , "Trigger_validation_cut/O" );

  tr_original->SetBranchAddress	( "Image_cut", &bl_image_cut );
  tr_new->Branch		( "Image_cut", &bl_image_cut , "Image_cut/O" );

  for( int i=0; i<tr_original->GetEntries(); i++ )
    {
      tr_original->GetEntry(i);

      if( im > 2.5 )
	tr_new->Fill();
    }

  tf->WriteObject( tr_new , tr_new->GetName() );
  tf->Close();
}
