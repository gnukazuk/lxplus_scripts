#!/bin/bash

if [ $# -ne 1 ]
then
    echo "Usage $0 [path to data in castor]"
    exit
fi

data_path=$1
data_name="`basename ${data_path}`"

nsls $1 > /dev/null 2> /dev/null


if [ $? -ne 0 ]
then
    echo -e "${data_path} doesn't exist"
    exit
fi

echo rfcp ${data_path} .

rfcp ${data_path} ~/workspace/temp > /dev/null 2> /dev/null
md5sum ~/workspace/temp/${data_name} > ~/workspace/temp/${data_name}_MD5

echo `basename ${data_path}`
