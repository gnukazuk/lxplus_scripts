#!/bin/bash

function MakeBatchJobScript_Header()
{

    if [ $# -ne 1 ]
    then
	echo -e "$0 [script file]"
	echo -r "Script is stopped"
	exit
    fi

    script=$1

    echo "#!/bin/bash" > ${script}
    echo  >> ${script} 

    # start up, initialization
    echo "cd /afs/cern.ch/user/g/gnukazuk/" >> ${script}
    echo "source .bash_profile" >> ${script}
    echo "source .bashrc" >> ${script}
    echo  >> ${script} 
    echo "source  /afs/cern.ch/user/g/gnukazuk/public/tgeant/setEnv.sh" >> ${script}
    echo  >> ${script} 
}
