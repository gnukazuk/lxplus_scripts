#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>

using namespace std;

/*
  This is for nssize.sh
  This 
    * read file and get a size of file
    * show total size of files
 */

int main( int argc, char* argv[])
{

  // Usage: nssize $(path to the list) $(flag)
  // $(path to the list) : path
  // $(flag) : If true, show a size of each file
  if( argc < 2 )
    {

      cerr << "nssize" << endl;
      cerr << "A number of argument is " << argc - 1 << endl;
      cerr << "This must be 1" << endl;
      cerr << "Program is stopped" << endl;
    }

  // get a path to the list
  string file = argv[1];

  // sed flag
  bool bl_show = false;
  if( argc == 3 && (string)argv[2] == "true")
    bl_show = true;

  ifstream ifs( file.c_str() );

  string stemp;
  double dtemp;

  // total size of files
  double sum = 0;

  // a number of file in a list
  int counter = 0;

  // read the list line by line
  while( ifs >> stemp >> stemp >> stemp >> stemp >> dtemp >> stemp >> stemp >> stemp >> stemp )
    {

      // name of file in this line 
      string file_name = stemp;

      // thrown away remains
      getline( ifs , stemp );

      // sum up a size
      sum += dtemp;

      // if show option is true      
      if( bl_show )
	{
	  cout << setw(5) << counter << " " 
	       << setw(30) << file_name << "  " ;
	  
	  if( dtemp < 1e3 ) // byte
	    cout << setw(4) << setprecision(3) << dtemp << "byte" << endl;
	  else if( dtemp < 1e6 ) // kB
	    cout << setw(4) << setprecision(3) << dtemp / 1e3 << " kB" << endl;
	  else if( dtemp < 1e9 ) // MB
	    cout << setw(4) << setprecision(3) << dtemp / 1e6 << "  MB" << endl;
	  else if( dtemp < 1e12 ) // GB
	    cout << setw(4) << setprecision(3) << dtemp / 1e9 << "   GB" << endl;
	  else if( dtemp < 1e15 ) // TB
	    cout << setw(4) << setprecision(3) << dtemp / 1e12 << "    TB" << endl;
	}
      
      counter++;
    }

  // print the result  
  cout << "total size is : " 
       << setprecision(5) << sum << " byte = ";

  if( sum < 1e9 ) // MB
    cout << setprecision(3) << sum / 1e6 << " MB" << endl;
  else if( sum < 1e12 ) // GB
    cout << setprecision(3) << sum / 1e9 << " GB" << endl;
  else if( sum < 1e15 ) // TB
    cout << setprecision(3) << sum / 1e12 << " TB" << endl;
  else
    cout << endl;

  cout << "A number of file is " << counter+1 << endl;
  return 0;
}
