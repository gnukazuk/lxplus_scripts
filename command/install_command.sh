#/bin/bash

##################################################
#   install_command.sh                           #
# ---------------------------------------------- #
# This script installs scripts in this directory.#
# This must be executed in ~/script/command dir. #
# ---------------------------------------------- #
# ver1 2016/02/05 Genki NUKAZUKA                 #
##################################################

# get the name of this file without a path to the current dir
this_file=`echo $0 | sed -e "s|./||g"`

# loop over all .sh files except this file
for file in `ls *.sh | grep -v ${this_file}`
do

    # define a command    
    command=`basename ${file} .sh`
    here=${PWD}

    # if command ( symbolic link in ~/bin ) doesn't exist,
    # create it
    if [ ! -e "${HOME}/bin/${command}" ]
    then
	cd ~/bin
	ln -s ${here}/${file}  ${command}
	echo -e "${command} is installed"
	cd - > /dev/null
    fi
done
