#!/bin/bash

root_path=/afs/cern.ch/sw/lcg/app/releases/ROOT/5.3*/i686-slc*/root/bin/g2root

g2rts=(`ls ${root_path}`)

if [ "$#" -ne 0 ]
then
    echo "Usage: $0"
    echo "No argument is required !" 
    echo "detectors.C will be created from detectors.rz"
    echo "You cannot specify name of input file and output file"
    exit
fi

for g2rt in ${g2rts[@]}
do

    thisroot=${g2rt%/*}/thisroot.sh

   if [ -e ${thisroot} ]
	then
	echo ========================
	echo $g2rt
	echo $thisroot
	echo

	source $thisroot
	. $thisroot 
	${g2rt} detectors.rz
   fi

   echo -e "Continue? (type y/n)"
   read ans

   if [ "${ans}" = "n" ]
   then
       exit 0
   fi

done

#NOW=`pwd`
source ~/.bash_profile
