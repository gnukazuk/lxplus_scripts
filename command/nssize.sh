#/bin/bash

##########################################
# nssize.sh                               #
# ------------------------------------------ #
# Return total size in the castor dir.   #
# 1st arg: path to castor dir.           #
# 2nd arg: show size of each file ot not #
# -------------------------------------- #
# ver1 2015/02/05 Genki NUKAZUKA         #
# ver2 2015/02/05 21:30 Genki NUKAZUKA   #
#    C++ program is used to calcurate    #
##########################################

if [ $# -eq 0 ]
then
    echo -e "Usage: $0 [path to castor directory] [show-each]" >&2
    echo -e "[path to castor directory] : mandatory" >&2
    echo -e "[show-each]: 0-no, 1-yes(default)" >&2
    exit -1
else
    if [ "$2" = "1" ]
    then
	flag_show=1
    else
	flag_show=0
    fi
fi

nsls $1 >/dev/null 2>/dev/null
if [ "$?"  -ne 0 ]
then
    echo -e "$1 doesn't exist"
    exit
fi

nsls -l $1 > $$

if [ "${flag_show}" -eq 1 ] 
then
    /afs/cern.ch/user/g/gnukazuk/script/src/nssize $$ true
else
    /afs/cern.ch/user/g/gnukazuk/script/src/nssize $$ false
fi

rm $$
