#!/bin/bash

if [ $# -ne 1 ]
then
    echo -e  "$0 [root dir of coral]"
    echo -e "candidate:"
    echo -e "`ls -d1 ~/public/coral/* | /bin/grep -v .sh | xargs -I {} basename {} | /bin/grep coral`"
    echo -e "choose one of them"
    exit
fi

corals_dir=${HOME}/public/coral
coral_root_dir=${corals_dir}/$1
if [ ! -e "${coral_root_dir}" ]
then
    echo -e "${coral_root_dir} is not found"
    echo -e "candidate:"
    echo -e "`ls -d1 ~/public/coral/* | /bin/grep -v .sh | xargs -I {} basename {} | /bin/grep coral`"
    echo -e "choose one of them"
    exit
fi


pcms=(
    "${coral_root_dir}/src/geom/gem/CsGEMPlaneDict_rdict.pcm" \
	"${coral_root_dir}/src/geom/mumega/CsMumegaPlaneDict_rdict.pcm" \
	"${coral_root_dir}/src/geom/gem/CsPixelGEMPlaneDict_rdict.pcm" \
	"${coral_root_dir}/src/geom/mumega/CsPixelMumegaPlaneDict_rdict.pcm" \
	"${coral_root_dir}/src/rich/evdis/CsRCEvdisDict_rdict.pcm" \
	"${coral_root_dir}/src/geom/mumega/CsRectPixelMumegaPlaneDict_rdict.pcm"  \
	)

for file in ${pcms[@]}
do
    ln -s $file 
done
