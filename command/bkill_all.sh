#!/bin/bash

bjobs | cut -c1-9 > $$

while read line
do
    
    if [ "${line}" != "JOBID" ]
	then
	echo bkill $line
	bkill $line
    fi

done<$$

rm $$
